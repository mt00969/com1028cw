package com.com1028.cw.mt00969;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RequirementTests {

	private Connection connect;
	private Statement statement;

	@Before
	public void setUp() throws SQLException {

		String db = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

		try {

			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			connect = DriverManager.getConnection(db, "root", "");

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@Test
	public void TestReq1() throws SQLException {
		String Expected = "";
		String Actual = "";
		String Result = "";
		String print = "";
		int counter = 0;

		try {

			String query = "select * from customers where salesRepEmployeeNumber is null ORDER BY customerNumber asc";
			statement = connect.createStatement();

			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				Result += "\nCustomerName: " + results.getString("customerName") + "\n" + "ContactFullName: "
						+ results.getString("contactFirstName") + results.getString("contactLastName") + "\n" + "City: "
						+ results.getString("city") + "\n" + "PhoneNumber: " + results.getString("phone") + "\n";

				counter++;

				print = "\n" + counter + " records";

				Expected = Result.concat(print);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		Customer.getData();
		Actual = Customer.allCustomersWithNoSR();

		System.out.println("\n*****Requirement Set F Task 1*****");

		System.out.println("\n----- Report the name and city of customers who don't have sales representatives ----- ");

		System.out.println(Expected + "\n");

		System.out.println(
				"End of printing...\n------------------------------------------------------------------------------------------------------------\n");

		assertEquals(Expected, Actual);
		assertEquals(Customer.getCounter(), counter);

	}

	@Test
	public void TestReq2() throws SQLException {
		String Expected = "";
		String Actual = "";
		String Result = "";
		String print = "";
		int counter = 0;

		try {

			String query = "select customers.customerNumber, customers.contactFirstName, customers.contactLastName, customers.customerName, format(payments.amount,2) as amount , count(*) over() total from customers inner join payments on payments.customerNumber=customers.customerNumber where amount > 100000 order by payments.amount desc";
			statement = connect.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				Result += "CustomerNumber: " + results.getInt("customers.customerNumber") + "\n" + "CustomerName: "
						+ results.getString("customerName") + "\n" + "ContactFullName: "
						+ results.getString("contactFirstName") + " " + results.getString("contactLastName") + "\n"
						+ "Amount: $" + results.getString("amount") + "\n\n";

				counter = results.getInt("total");

				print = counter + " records";

				Expected = Result.concat(print);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		Payment.getData();
		Actual = Payment.allPaymentsOverHundredThousand();
		System.out.println("*****Requirement Set F Task 2*****");

		System.out.println(
				"\n----- Report those payments greater than $100,000. -----\n----- Sort the report so the customer who made the highest payment appears first. ----- \n");

		System.out.println(Expected + "\n");

		System.out.println(
				"End of printing...\n------------------------------------------------------------------------------------------------------------\n");

		assertEquals(Expected, Actual);
		assertEquals(Payment.getCounter(), counter);
	}

	@Test
	public void TestReq3() throws SQLException {
		String Expected = "";
		String Actual = "";
		String Result = "";
		String print = "";
		String pattern = "##.00";
		int counter = 0;
		DecimalFormat df = new DecimalFormat(pattern);

		try {

			String query = "select customers.customerNumber, customers.customerName, round(avg(TIMESTAMPDIFF(DAY,orderDate,shippedDate)),2) as 'AverageDeliveryTimeInDays', count(*) over() totalNoRows from orders inner join customers on customers.customerNumber = orders.customerNumber where shippedDate is not null group by orders.customerNumber order by AverageDeliveryTimeInDays desc, customerNumber asc";

			statement = connect.createStatement();

			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				Result += "CustomerName: " + results.getString("customerName") + "\n" + "CustomerNumber: "
						+ results.getString("customerNumber") + "\n" + "AverageDeliveryTime: "
						+ df.format(results.getDouble("AverageDeliveryTimeInDays")) + " day(s)\n" + "\n";

				counter = results.getInt("totalNoRows");

				print = counter + " records\n";

				Expected = Result.concat(print);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		Order.getData();
		Actual = Order.allOrdersAverageDeliveryTime();

		System.out.println("*****Requirement Set F Task 3*****");

		System.out.println(
				"\n----- Compute the average time between order date and ship date for each customer. -----\n----- Order it by the largest difference ----- \n");

		System.out.println(Expected);

		System.out.println(
				"End of printing...\n------------------------------------------------------------------------------------------------------------");

		assertEquals(Expected, Actual);
		assertEquals(Order.getSortedMap().size(), counter);

	}

	@After
	public void tearDown() throws SQLException {
		connect.close();
	}

}
