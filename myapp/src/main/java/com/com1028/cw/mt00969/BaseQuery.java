package com.com1028.cw.mt00969;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseQuery {

	protected static Connection connect;
	private final String database = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

	public BaseQuery(String uname, String pwd) {
		super();
		try {

			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			connect = DriverManager.getConnection(database, uname, pwd);
			System.out.println(
					"\nlogin successful\n------------------------------------------------------------------------------------------------------------");
		} catch (Exception e) {
			System.out.println("PLEASE TRY AGAIN");
			System.out.println(e);
		}
	}

	protected static ResultSet useTable(String tableName) throws SQLException {
		String query = "select * from " + tableName;
		Statement s = connect.createStatement();
		ResultSet rs = s.executeQuery(query);
		return rs;
	}

	public static void main(String args[]) {

		Customer.getData();
		Customer.showAllCustomersWithNoSR();

		Payment.getData();
		Payment.showAllPaymentsOverHundredThousand();

		Order.getData();
		Order.showAllOrdersAverageDeliveryTime();

	}
}
