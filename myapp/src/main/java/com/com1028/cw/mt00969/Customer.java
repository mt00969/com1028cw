package com.com1028.cw.mt00969;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Customer {

	private int customerNumber = 0;
	private String customerName = null;
	private String contactFirstName = null;
	private String contactLastName = null;
	private String city = null;
	private String phone = null;
	private int salesRepEmployeeNumber = 0;
	private static int counter;
	private static List<Customer> customers = new ArrayList<Customer>();

	public Customer(int customerNumber, String customerName, String contactFirstName, String contactLastName,
			String city, String phone, int salesRepEmployeeNumber) {
		super();
		this.customerNumber = customerNumber;
		this.customerName = customerName;
		this.contactFirstName = contactFirstName;
		this.contactLastName = contactLastName;
		this.city = city;
		this.phone = phone;
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;

	}

	public static void getData() {
		BaseQuery connection = new BaseQuery("root", "");

		ResultSet rs;
		try {
			rs = BaseQuery.useTable("customers");

			while (rs.next()) {
				int customerNumber = rs.getInt("customerNumber");
				String customerName = rs.getString("customerName");
				String customerFirstName = rs.getString("contactFirstName");
				String customerLastName = rs.getString("contactLastName");
				String city = rs.getString("city");
				String phone = rs.getString("phone");
				int salesRepEmployeeNumber = rs.getInt("salesRepEmployeeNumber");
				customers.add(new Customer(customerNumber, customerName, customerFirstName, customerLastName, city,
						phone, salesRepEmployeeNumber));
			}

			BaseQuery.connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static String allCustomersWithNoSR() {

		String out = "";

		for (Customer c : customers) {
			if (c.getSalesRepEmployeeNumber() == 0) {

				out += "\nCustomerName: " + c.getCustomerName() + "\n" + "ContactFullName: " + c.getContactFirstName()
						+ c.getContactLastName() + "\n" + "City: " + c.getCity() + "\n" + "PhoneNumber: " + c.getPhone()
						+ "\n";
				counter++;
			}

		}

		return out + "\n" + counter + " records";

	}

	public static void showAllCustomersWithNoSR() {

		System.out.println("\n*****Requirement Set F Task 1*****");

		System.out.println("\n----- Report the name and city of customers who don't have sales representatives ----- ");

		System.out.println(Customer.allCustomersWithNoSR());

		System.out.println(
				"\nEnd of printing...\n------------------------------------------------------------------------------------------------------------\n");

	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public String getCity() {
		return city;
	}

	public String getPhone() {
		return phone;
	}

	public int getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}

	public static int getCounter() {
		return counter;
	}

	public static List<Customer> getCustomers() {
		return customers;
	}

}
