package com.com1028.cw.mt00969;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Payment {

	private int customerNumber = 0;
	private double amount = 0;
	private static int counter;
	private static List<Payment> payments = new ArrayList<Payment>();

	private static Comparator<Payment> orderByAmount = new Comparator<Payment>() {

		@Override
		public int compare(Payment p1, Payment p2) {
			double amount1 = p1.getAmount();
			double amount2 = p2.getAmount();
			return (int) (amount2 - amount1);
		}

	};

	public Payment(int customerNumber, double amount) {
		super();
		this.customerNumber = customerNumber;
		this.amount = amount;
	}

	public static void getData() {
		BaseQuery connection = new BaseQuery("root", "");

		ResultSet rs;
		try {

			rs = BaseQuery.useTable("payments");

			while (rs.next()) {

				int customerNumber = rs.getInt("customerNumber");
				double amount = rs.getDouble("amount");
				payments.add(new Payment(customerNumber, amount));

			}

			BaseQuery.connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static String allPaymentsOverHundredThousand() {
		String out = "";
		String pattern = "###,###.00";
		DecimalFormat df = new DecimalFormat(pattern);
		for (Payment p : getSortedPayments()) {

			for (Customer c : Customer.getCustomers()) {

				if (p.getAmount() > 100000) {
					if (c.getCustomerNumber() == p.getCustomerNumber()) {

						out += "CustomerNumber: " + p.getCustomerNumber() + "\n" + "CustomerName: "
								+ c.getCustomerName() + "\n" + "ContactFullName: " + c.getContactFirstName() + " "
								+ c.getContactLastName() + "\n" + "Amount: $" + df.format((p.getAmount())) + "\n\n";
						counter++;
					}
				}
			}
		}

		return out + counter + " records";

	}

	public static void showAllPaymentsOverHundredThousand() {
		System.out.println(
				"\nEnd of printing...\n------------------------------------------------------------------------------------------------------------\n");

		System.out.println("*****Requirement Set F Task 2*****");

		System.out.println(
				"\n----- Report those payments greater than $100,000. -----\n----- Sort the report so the customer who made the highest payment appears first. ----- \n");

		System.out.println(allPaymentsOverHundredThousand());

		System.out.println(
				"\nEnd of printing...\n------------------------------------------------------------------------------------------------------------\n");

	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public double getAmount() {
		return amount;
	}

	public static int getCounter() {
		return counter;
	}

	public static List<Payment> getPayments() {
		return payments;
	}

	public static List<Payment> getSortedPayments() {
		List<Payment> sortedPayments = getPayments();
		Collections.sort(sortedPayments, Payment.orderByAmount);
		return sortedPayments;
	}

}
