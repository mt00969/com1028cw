package com.com1028.cw.mt00969;

import static java.lang.Math.toIntExact;

import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Order {

	private int orderNumber;
	private int customerNumber;
	private Date orderDate;
	private Date shippedDate;
	private double dateDiff;
	private static List<Order> orders = new ArrayList<Order>();
	private static Map<Integer, Double> sortedMap = new HashMap<Integer, Double>();

	public Order(int orderNumber, int customerNumber, Date orderDate, Date shippedDate, double dateDiff) {
		super();
		this.orderNumber = orderNumber;
		this.customerNumber = customerNumber;
		this.orderDate = orderDate;
		this.shippedDate = shippedDate;
		this.dateDiff = dateDiff;
	}

	public static void getData() {
		BaseQuery connection = new BaseQuery("root", "");
		ResultSet rs;

		try {

			rs = BaseQuery.useTable("orders");

			while (rs.next()) {

				int orderNumber = rs.getInt("orderNumber");
				int customerNumber = rs.getInt("customerNumber");
				Date orderDate = rs.getDate("orderDate");
				Date shippedDate = rs.getDate("shippedDate");

				if (shippedDate != null) {

					int dateDiff = toIntExact(
							(rs.getDate("shippedDate").getTime() - rs.getDate("orderDate").getTime()) / 86400000);
					orders.add(new Order(orderNumber, customerNumber, orderDate, shippedDate, dateDiff));
				}

			}
			BaseQuery.connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static Map<Integer, Double> sortByValue(Map<Integer, Double> map) {

		List<Map.Entry<Integer, Double>> listOfOrders = new LinkedList<Map.Entry<Integer, Double>>(map.entrySet());

		Collections.sort(listOfOrders, new Comparator<Map.Entry<Integer, Double>>() {
			@Override
			public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
				return (o1.getKey()).compareTo(o2.getKey());
			}
		});

		Collections.sort(listOfOrders, new Comparator<Map.Entry<Integer, Double>>() {
			@Override
			public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		HashMap<Integer, Double> sortedOrders = new LinkedHashMap<Integer, Double>();
		for (Map.Entry<Integer, Double> aa : listOfOrders) {
			sortedOrders.put(aa.getKey(), aa.getValue());
		}

		return sortedOrders;
	}

	public static String allOrdersAverageDeliveryTime() {

		int counter = 0;
		String pattern = "###.00";
		String out = "";
		DecimalFormat df = new DecimalFormat(pattern);
		df.setRoundingMode(RoundingMode.HALF_UP);

		Map<Integer, Double> unsortedMap = orders.stream().collect(
				Collectors.groupingBy(p -> p.getCustomerNumber(), Collectors.averagingDouble(p -> p.getDateDiff())));

		sortedMap = sortByValue(unsortedMap);

		for (Map.Entry<Integer, Double> e : sortedMap.entrySet()) {
			for (Customer c : Customer.getCustomers()) {
				if (e.getKey() == c.getCustomerNumber()) {

					out += "CustomerName: " + c.getCustomerName() + "\n" + "CustomerNumber: " + e.getKey() + "\n"
							+ "AverageDeliveryTime: " + df.format(e.getValue()) + " day(s)\n" + "\n";

					counter++;
				}
			}

		}
		return out + counter + " records\n";
	}

	public static void showAllOrdersAverageDeliveryTime() {
		System.out.println("*****Requirement Set F Task 3*****");

		System.out.println(
				"\n----- Compute the average time between order date and ship date for each customer. -----\n----- Order it by the largest difference ----- \n");

		System.out.println(allOrdersAverageDeliveryTime());

		System.out.println(
				"End of printing...\n------------------------------------------------------------------------------------------------------------");

	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public double getDateDiff() {
		return dateDiff;
	}

	public static List<Order> getOrders() {
		return orders;
	}

	public static Map<Integer, Double> getSortedMap() {
		return sortedMap;
	}

}
